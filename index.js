require("dotenv").config();
const discord = require("discord.js");
const neko = require("nekos.life");
const client = new discord.Client({
	intents: [
		"GUILDS",
		"GUILD_MEMBERS",
		"GUILD_EMOJIS_AND_STICKERS",
		"GUILD_BANS",
		"GUILD_MESSAGES",
		"GUILD_MESSAGE_REACTIONS",
		"GUILD_PRESENCES",
		"GUILD_WEBHOOKS",
		"GUILD_VOICE_STATES",
		"DIRECT_MESSAGES",
		"DIRECT_MESSAGE_REACTIONS",
		"GUILD_INVITES"
	]
});
const prefix = "."

client.on("ready", () => {
	console.log("I am ready!");
});

client.on("messageCreate", async (message) => {
	if (message.content === prefix+"ping") {
		message.reply("pong");
	}
	if (message.content.split()[0] === prefix+"warn") {
		message.reply("pong");
	}
	// if statement that checks if the message starts with prefix and has the word "ban" in it, then checks if the user has privileges to ban and then bans the user id specified
	if (message.content.startsWith(prefix+"ban") && message.member.hasPermission("BAN_MEMBERS")) {
		const user = message.mentions.users.first();
		const member = message.guild.member(user);
		if (user) {
			member.ban({
				reason: "They were bad!"
			}).then(() => {
				message.reply(`${user.tag} has been banned!`);
			}).catch(err => {
				message.reply("I was unable to ban the user.");
				console.log(err);
			});
		} else {
			message.reply("You need to specify a user to ban!");
		}
	}
	// if statement that checks if the message starts with prefix and has the word "kick" in it, then checks if the user has privileges to kick and then kicks the user id specified
	if (message.content.startsWith(prefix+"kick") && message.member.hasPermission("KICK_MEMBERS")) {
		const user = message.mentions.users.first();
		const member = message.guild.member(user);
		if (user) {
			member.kick({
				reason: "They were bad!"
			}).then(() => {
				message.reply(`${user.tag} has been kicked!`);
			}).catch(err => {
				message.reply("I was unable to kick the user.");
				console.log(err);
			});
		} else {
			message.reply("You need to specify a user to kick!");
		}
	}
	// if the message starts with prefix and has word embed fail in it, then it will send a message with an embed and an image
	if (message.content.startsWith(prefix+"embed fail")) {
		message.channel.send({
			embed: {
				title: "epic embed fail",
				description: "embed fail",
				color: 0x00ff00
				
			}
		});
	}
	// if message starts with prefix and has word sound in it, then it will send a message with a sound
	if (message.content.startsWith(prefix+"sound")) {
		message.channel.send("", {
			files: ["./sound.mp3"]
		});
	}
	//sorry
	if (message.content.startsWith(prefix+"image")) {
		message.channel.send(neko.goose());
	}
});


client.login(process.env.BOT_TOKEN);
